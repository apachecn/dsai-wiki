## 二、问题形式化

在本节中，我们首先介绍图嵌入中基本概念的定义，然后提供图嵌入问题的正式定义。

### 符号和定义

本综述中使用的符号的详细说明见表 1 。

**表1：**本文中使用的符号。

| 符号 | 说明 |  |
| --- | --- | --- |
| ![](img/img23.png) | 集合的基数 |  |
| ![](img/img24.png)  = ![](img/img25.png) | 带有节点 ![](img/img26.png)  和边 ![](img/img27.png) 的图 ![](img/img24.png)   |  |
| ![](img/img28.png)  =  ![](img/img29.png) | 图 ![](img/img24.png) 的子结构，其中  ![](img/img30.png) |  |
| ![](img/img13.png)  ， ![](img/img31.png) | 节点 ![](img/img32.png)  和连接 ![](img/img13.png)  和 ![](img/img34.png) 的边 ![](img/img33.png)   |  |
| ![](img/img35.png) | 接邻矩阵 ![](img/img24.png) |  |
| ![](img/img36.png) | 矩阵 A 的第 i 行向量   |
| ![](img/img38.png) | 矩阵 A 的第 i 行第 j 列 |  |
| ![](img/img40.png)  ，  ![](img/img41.png) | 节点 ![](img/img13.png) 类型和边 ![](img/img31.png) 的类型 |  |
| ![](img/img42.png)  ，  ![](img/img43.png) | 节点类型集和边类型集 |  |
| ![](img/img44.png) | 节点 ![](img/img13.png) 的k个最近邻居 |  |
| ![](img/img45.png) | 特征矩阵，每行 ![](img/img46.png) 是 ![](img/img13.png) 的 ![](img/img47.png) 维向量  |  |
| ![](img/img48.png)  ， ![](img/img49.png)  ，  ![](img/img50.png) | 节点 ![](img/img13.png) ，边 ![](img/img31.png) 和结构  ![](img/img28.png) 的嵌入 |  |
| ![](img/img51.png) | 嵌入的维度 |  |
| `<h, r, t>` | 知识图三元组，具有头部实体 ![](img/img53.png)  ， |  |
|  | 尾部实体 ![](img/img54.png) 以及他们之间的关系 ![](img/img55.png) |
| ![](img/img56.png)  ，  ![](img/img57.png) | 节点 ![](img/img13.png) 和 ![](img/img34.png) 之间的一阶和二阶邻近度  |  |
| ![](img/img58.png) | 信息级联 |  |
| ![](img/img59.png) | 拥有级联 ![](img/img58.png) 的级联图 |  |

图是 ![](img/img24.png) = ![](img/img25.png)，其中 ![](img/img60.png) 是一个节点，![](img/img61.png) 是一个边。![](img/img24.png) 关联节点类型的映射函数 ![](img/img62.png) 和边类型的映射函数 ![](img/img63.png)。

![](img/img42.png) 和 ![](img/img43.png) 分别表示节点类型和边类型的集合。 每个节点 ![](img/img32.png) 属于一种特定类型，即 ![](img/img64.png)。 同样，对于 ![](img/img33.png)，![](img/img65.png)。

同构图 ![](img/img66.png) =_ ![](img/img25.png) 是一个图，满足 ![](img/img67.png)。![](img/img24.png) 中的所有节点属于单一类型，所有边都属于单一类型。

异构图 ![](img/img68.png) = ![](img/img25.png) 是一个图，满足 ![](img/img69.png) 和/或 ![](img/img70.png)。

知识图 ![](img/img71.png) = ![](img/img25.png) 是一个有向图，其节点是实体 ，边是主体 - 属性 - 客体三元组。形式为（ 头部实体 ， 关系，尾部实体）的每个边（表示为 ![](img/img52.png)）表示关系 ![](img/img55.png) 来自实体 ![](img/img53.png) 到实体 ![](img/img54.png)。

![](img/img72.png) 是实体，![](img/img73.png) 是关系。 在本综述中，我们将 ![](img/img52.png) 称作知识图三元组。 例如，在图 3 中，有两个三元组：![](img/img74.png) 和 ![](img/img75.png)。 请注意，知识图中的实体和关系通常具有不同的类型[14,15]。因此，知识图可以被视为异构图的实例。

![](img/fig3.jpg)

**图3：**知识图的玩具示例。

通常采用以下邻近度量来量化要在嵌入空间中保留的图属性。 一阶邻近度是仅由边连接的节点之间的局部成对相似性。 它比较节点对之间的直接连接强度。 从形式上看，

节点 ![](img/img13.png) 和节点 ![](img/img34.png) 之间的一阶邻近度是边 ![](img/img31.png) 的权重，即 ![](img/img38.png)。

如果两个节点由具有较大权重的边连接，则它们更相似。![](img/img56.png) 表示节点 ![](img/img13.png) 和 ![](img/img34.png) 之间的一阶邻近度， 我们有 ![](img/img77.png)。 让  ![](img/img78.png) 表示 ![](img/img13.png) 和其他节点的一阶邻近度。 以图 1（a）中的图为例，![](img/img2.png) 和 ![](img/img3.png) 的一阶邻近度是边的权重 ![](img/img79.png)，表示为 ![](img/img80.png)。![](img/img81.png) 记录 ![](img/img2.png) 和图中的其他节点的边的权重，即  ![](img/img82.png)。

二阶邻近度比较节点的邻域结构的相似性。 两个节点的邻域越相似，它们之间的二阶邻近度越大。从形式上看，

节点 ![](img/img13.png) 和 ![](img/img34.png)之间的二阶邻近度 ![](img/img57.png) 是 ![](img/img13.png) 的邻居_ _![](img/img83.png) 和 ![](img/img34.png) 的邻居 ![](img/img84.png) 的相似度。

再次，以图[1（a）](#fig:ig)为例： ![](img/img85.png) 是 ![](img/img81.png) 和 ![](img/img86.png) 的相似度。 如前所述，  ![](img/img82.png) 并且 ![](img/img87.png) ![](img/img88.png)。 让我们考虑余弦相似度 ![](img/img89.png) 和 ![](img/img90.png)。 我们可以看到 ![](img/img2.png) 和 ![](img/img91.png) 之间的邻近度等于零，![](img/img2.png) 和 ![](img/img91.png) 没有任何共同邻居。 ![](img/img2.png) 和 ![](img/img3.png) 有一个共同的邻居 ![](img/img4.png)，因此他们的二阶邻近度 ![](img/img85.png) 大于零。

可以同样定义更高阶的邻近度 。 例如，节点 ![](img/img13.png) 和 ![](img/img34.png) 之间的 K 阶邻近度是 ![](img/img94.png) 和  ![](img/img95.png) 的相似度。 请注意，有时高阶邻近度也使用其他一些指标来定义，例如 Katz Index，Rooted PageRank，Adamic Adar 等[11]。

值得注意的是，在一些工作中，一阶和二阶邻近度是基于两个节点的联合概率和条件概率凭经验计算的。 更多细节将在 4.3.2 中讨论。

图嵌入：给定图的输入 ![](img/img24.png) = ![](img/img25.png)，以及嵌入的预定义维度 ![](img/img51.png) （![](img/img96.png)），图嵌入的问题是，将 ![](img/img24.png) 转换为一个 ![](img/img51.png) 维空间，其中尽可能保留图属性。可以使用诸如一阶和更高阶邻近度来量化图特性。每个图都表示为 ![](img/img51.png) 维向量（对于整图）或一组 ![](img/img51.png) 维向量，每个向量表示图的一部分的嵌入（例如，节点，边，子结构）。

图1显示了嵌入图的玩具示例 ![](img/img97.png)  。 给定一个输入图（图[1（a）](#fig:ig) ），图嵌入算法用于将节点（图[1（b）](#fig:ne) ）/边（图[1（c）](#fig:ee) ），子结构（图[1（d））](#fig:se) ）/整图（图[1（e）](#fig:we) ）转换为2D向量（即2D空间中的点）。 在接下来的两节中，我们分别基于问题设定和嵌入技术，对图嵌入文献进行分类，提供了两种图嵌入分类法。

